﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class ButtonObjectSelection : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {
    public Interactable interactableToSpawn;
    private LayerMask mask;

    private Vector3 initialPosition;
    private Vector3 spawnPosition;
    private Quaternion spawnRotation;
    private RaycastHit hit;
    private Button button;
    public List<Material> previewMaterials = new List<Material>();

    private void Awake() {
        button = GetComponent<Button>();
        mask = LayerMask.GetMask("Terrain");
        initialPosition = interactableToSpawn.transform.position;

        PopulatePreviewMaterials();
    }

    private void Update() {
        UpdatePreviewMaterial();
    }

    public void OnPointerDown(PointerEventData eventData) {
        SetObjectLayer(interactableToSpawn.transform, "Overlay");

        button.interactable = false;

        MoveAlongOverlay();
    }

    public void OnDrag(PointerEventData eventData) {
        RaycastFindTerrain();
    }

    public void OnPointerUp(PointerEventData eventData) {
        if (hit.transform && DetectOverlap.overlaps.Count == 0) {
            Transform t = Instantiate(interactableToSpawn, hit.point, spawnRotation, MyTracker.instance.transform).transform;
            t.localScale = interactableToSpawn.transform.localScale;
            SetObjectLayer(t, "Default");

            CopyMaterialsToNewIntsanceWithDefaultColour(t);
            Destroy(t.GetComponent<DetectOverlap>());
        }

        RecolourMaterialsInHierarchy(Color.white);
        hit = new RaycastHit();

        SetObjectLayer(interactableToSpawn.transform, "Overlay");

        button.interactable = true;
        interactableToSpawn.transform.position = initialPosition;

    }

    private void SetObjectLayer(Transform _transform, string _layerName) {
        Transform[] transforms = _transform.GetComponentsInChildren<Transform>();
        foreach (Transform t in transforms) {
            t.gameObject.layer = LayerMask.NameToLayer(_layerName);
        }
    }

    private void PopulatePreviewMaterials() {
        Renderer[] renderers = interactableToSpawn.gameObject.GetComponentsInChildren<Renderer>();

        foreach (Renderer r in renderers) {
            foreach (Material m in r.materials) {
                previewMaterials.Add(m);
            }
        }
    }

    private void RaycastFindTerrain() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 50.0f, mask)) {
            MoveAlongTerrain();
        } else {
            MoveAlongOverlay();
        }
    }

    private void MoveAlongTerrain() {
        SetObjectLayer(interactableToSpawn.transform, "Default");

        spawnPosition = hit.point;
        interactableToSpawn.transform.position = spawnPosition;

        spawnRotation = Quaternion.FromToRotation(interactableToSpawn.transform.up, hit.normal) * interactableToSpawn.transform.rotation;
        interactableToSpawn.transform.rotation = spawnRotation;
    }

    private void MoveAlongOverlay() {
        SetObjectLayer(interactableToSpawn.transform, "Overlay");

        Vector3 pos = Input.mousePosition;
        pos.z = transform.forward.z - Camera.main.transform.forward.z + 3;

        interactableToSpawn.transform.position = Camera.main.ScreenToWorldPoint(pos);
        interactableToSpawn.transform.rotation = Camera.main.transform.rotation;
    }

    private void UpdatePreviewMaterial() {
        if (previewMaterials == null || previewMaterials.Count <= 0) { return; }

        if (hit.transform) {
            RecolourMaterialsInHierarchy((DetectOverlap.overlaps.Count > 0) ? Color.red : Color.green);
        } else {
            RecolourMaterialsInHierarchy(Color.white);
        }
    }

    private void RecolourMaterialsInHierarchy(Color _colour) {
        foreach (Material m in previewMaterials) {
            m.color = _colour;
        }
    }

    private void CopyMaterialsToNewIntsanceWithDefaultColour(Transform _transform) {
        Renderer[] renderers = _transform.gameObject.GetComponentsInChildren<Renderer>();

        foreach (Renderer r in renderers) {
            for (int i = 0; i < r.materials.Length; i++) {
                r.materials[i] = new Material(r.materials[i]);
                r.materials[i].color = Color.white;
            }
        }
    }
}
