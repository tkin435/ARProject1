﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cans : Interactable
{
	public float rotateSpeed;
	public new AudioSource audio;
	private bool isDown;

	protected override void OnMouseDown()
	{
		Respond();
	}

	protected override void Respond()
	{
		isDown = !isDown;

		audio = GetComponent<AudioSource>();
		audio.PlayOneShot(audio.clip);
		audio.Play();

	}

	private void Update()
	{

		//	//Hard code speed...Can change in inspector.

		//	///Lerp logic where if grounded perform transform rotation

			if (isDown) { //isDown  == True
				transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(10,20,50)), rotateSpeed * Time.deltaTime);
				Debug.Log(rotateSpeed * Time.deltaTime);
			} else { //isDown false
				transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, 0), rotateSpeed * Time.deltaTime);
			}
		



	}


}











