﻿using UnityEngine;

public class CylinderInteract : Interactable {
	public ParticleSystem ps;

	private AudioSource audioSource;
	private ParticleSystem.EmissionModule em;

	private void Awake() {
		em = ps.emission;
		audioSource = GetComponent<AudioSource>();
	}

	protected override void OnMouseDown() {
		Respond();
	}

	protected override void Respond() {
		//Particle System Toggle;
		em.enabled = !em.enabled;

		audioSource = GetComponent<AudioSource>();
		audioSource.PlayOneShot(audioSource.clip);
		audioSource.Play();
	}

}
