﻿using UnityEngine;
using System.Collections;

public class Drumcan : Interactable {
	public float rotateSpeed;
	public new  AudioSource audio;

	private bool isDown;
	private Vector3 targetRotation;
	private Vector3 startRotation;
	private Coroutine corToggleState;

	private void Awake() {
		startRotation = transform.eulerAngles;
		targetRotation = transform.eulerAngles + (new Vector3(0,0,90));
	}

	protected override void OnMouseDown() {
		Respond();
	}

	protected override void Respond() {
		//Toggling down and is down, which is excecuted in the OnMouseDown() method.
		if (corToggleState == null) {
			corToggleState = StartCoroutine(ToggleState());
		}

		audio = GetComponent<AudioSource>();
		audio.PlayOneShot(audio.clip);
		audio.Play();
	}

	//private void Update() {
	//	//Hard code speed...Can change in inspector.
		
	//	///Lerp logic where if grounded perform transform rotation
		
	//	if (isDown) { //isDown  == True
	//		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(targetRotation), rotateSpeed * Time.deltaTime);
	//		Debug.Log(rotateSpeed * Time.deltaTime);
	//	} else { //isDown false
	//		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, 0), rotateSpeed * Time.deltaTime);
	//	}
	//}

	private IEnumerator ToggleState() {
		float t = 0;
		float duration = 0.3f;

		isDown = !isDown;
		Vector3 start = isDown ? startRotation : targetRotation;
		Vector3 end = isDown ? targetRotation : startRotation;

		while (t < duration) {
			transform.GetChild(0).GetChild(0).eulerAngles = Vector3.Lerp(start, end, t / duration);

			t += Time.deltaTime;
			yield return null;
		}

		corToggleState = null;
	}
}
