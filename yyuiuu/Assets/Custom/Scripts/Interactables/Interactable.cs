﻿using UnityEngine;

public abstract class Interactable : MonoBehaviour{
	/// <summary>
	/// Call Respond from here.
	/// </summary>
    protected abstract void OnMouseDown();

	/// <summary>
	/// Call this method from your user interaction.
	/// </summary>
    protected abstract void Respond();
}
