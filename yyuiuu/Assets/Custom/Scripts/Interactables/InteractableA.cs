﻿using System.Collections;
using UnityEngine;

public class InteractableA : Interactable {
    public AnimationCurve valueOverTime;

    private Vector3 initialScale;
    private Coroutine corResize;

    private void Awake() {
        initialScale = transform.localScale;
    }

    protected override void OnMouseDown() {
        Respond();
    }

    protected override void Respond() {
        if (corResize == null) {
            corResize = StartCoroutine(Resize());
        }
    }

    private IEnumerator Resize() {
        float t = 0;
        float duration = 0.5f;
        Vector3 growthScale = initialScale * 2;
		float percent = 0;

        while (t < duration) {
			percent = t / duration;
            transform.localScale = Vector3.Lerp(initialScale, growthScale, valueOverTime.Evaluate(percent));

            t += Time.deltaTime;
			yield return null;

			// THESE ARE ALL THE SAME.
			//yield return 0;
			//yield return null;
			//yield return new WaitForEndOfFrame();
			
			// wAITS FOR A PERDIOD OF TIME
			//yield return new WaitForSeconds(6);

			// THESE ARE ALL THE SAME.
			//yield return new WaitUntil(() => Input.GetButtonDown("Jump"));
			//yield return new WaitWhile(() => !Input.GetButtonDown("Jump"));

			//while (!Input.GetButtonDown("Jump")) {
			//	Debug.Log("Hurry up and jump man!");
			//  yield return null;
			//}
		}

		transform.localScale = initialScale;
        corResize = null;
    }
}
