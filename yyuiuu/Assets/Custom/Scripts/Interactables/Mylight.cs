﻿using System.Collections;
using UnityEngine;

public class Mylight : Interactable {
	public new AudioSource audio;
	public float speed = 10;

	private Vector3 startPos;
	private Vector3 endPos;
	private float percentage;
	private float startTime;

	protected Mylight() {
	}

	private void Start() {
		// Initializin the initial position of the asset
		startPos = transform.position;

		// Initializing the distance of the object which is set at a random (XYZ) values.
		endPos = transform.position 
			+ new Vector3(Random.Range(-30.0f, 110.0f), Random.Range(-100, 100f), Random.Range(-100f, 100f));
	}

	protected override void OnMouseDown() {
		// Calling the respond method for my mouse interactions. 
		Respond();
	}

	// Put mouse interaction here as it gets excecuted under the onMouseDown() method. 
	protected override void Respond() {
		audio = GetComponent<AudioSource>();
		audio.PlayOneShot(audio.clip);
		audio.Play();

		StartCoroutine(Move());
	}

	IEnumerator Move() {
		float minDistance = 3;

		while (Vector3.Distance(transform.position, endPos) > minDistance) {
			transform.position = Vector3.MoveTowards(transform.position, endPos, Time.deltaTime * speed);
			yield return null;
		}
	}
}
