﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sewage : Interactable {
	float lerpTime = 0f;
	float currentLerpTime;

	float moveDistance;

	Vector3 startPos;
	Vector3 endPos;

	bool grounded;

	private float startTime;

	public int x;
	public int y;
	public int z;




	protected override void OnMouseDown()
	{
		Respond();
	}



	protected void Start()
	{
		//Initiliazing start position
		startPos = transform.position;

		////initialize end position by adding the start position and (up= some garbage value plus the distance)
		endPos = transform.position + (transform.up * moveDistance);

		///Start time
		startTime = Time.time;

		/// Initializing the move distance; this is added to the endPos variable. 
		moveDistance = Vector3.Distance(startPos, endPos);


	}







	protected override void Respond()
	{
		//Toggling if the object is grounded or not. 
		grounded = !grounded;



		}


	void Update()
	{



		// Just a debug for the computer purpose///////////////////////////
		if (Input.GetKeyDown(KeyCode.Space))
		{

			
			currentLerpTime = 5f;

		}
		////Ignore the abode code



		//Increment lerptime with time
		currentLerpTime += Time.deltaTime;


		/////Checking the lerpTime with the current lerp
		if (currentLerpTime > lerpTime)
		{
			currentLerpTime = lerpTime;
		}

		////Initializing the distance covered
		float distanceCovered = lerpTime * (Time.deltaTime);



		///Initializing the percentage of the ate that the object interpolates
		float percentage = currentLerpTime / lerpTime;


		//// Assinging the transform postion to a lerp.

		//transform.position = Vector3.Lerp(startPos, endPos, percentage);





		if (grounded) // if grounded, start translation!
		{
			transform.Translate(new Vector3(x, y, z) * Time.deltaTime, Space.Self);

		}









		//Rotation Script
		//transform.Rotate(new Vector3(0, 0, 25), 1);
		///transform.rotation = Quaternion.Euler(new Vector3(0, 0, transform.rotation.eulerAngles.z + 90));


	}






	// Use this for initialization

}
