﻿using UnityEngine;
using Vuforia;

public class MyTracker : DefaultTrackableEventHandler, ITrackableEventHandler {
    public static MyTracker instance;

    private void Awake() {
        instance = this;
    }

    protected override void OnTrackingFound() {
        base.OnTrackingFound();

        Debug.LogError("<color=green>Yay!</color>");

        var rigidbodies = GetComponentsInChildren<Rigidbody>(true);
        foreach (var rb in rigidbodies) {
            rb.isKinematic = false;
        }
    }

    protected override void OnTrackingLost() {
        base.OnTrackingLost();

        Debug.LogError("<color=red>Argh!</color>");

        var rigidbodies = GetComponentsInChildren<Rigidbody>(true);
        foreach (var rb in rigidbodies) {
            rb.isKinematic = true;
        }
    }
}
