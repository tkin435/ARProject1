﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour {
	// Use this for initialization
	public bool isDown;
	public Vector3 startPosition;
	public float rotateSpeed;
	public int AngleZ;
	public int AngleX;
	public int AngleY;

	public void OnMouseDown()
	{
		//Making the button toggle;

		Debug.Log("am i being clicked?");
		isDown = !isDown;


	}

	// Update is called once per frame
	void Update () {
		rotateSpeed = 2f;

		

		if (isDown)
		{
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(AngleX,AngleY,AngleZ), rotateSpeed * Time.deltaTime);


		}
		else
		{
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, 0), rotateSpeed * Time.deltaTime);


		}







	}
}
