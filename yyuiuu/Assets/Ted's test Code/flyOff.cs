﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyOff : MonoBehaviour {
		Vector3 startPos;
		Vector3 endPos;
		public float speed;
		public float distanceCovered;
		private Vector3 moveDistance;
		private float percentage;
		private float startTime;


	private void Start()
	{
		startPos = transform.position;

		moveDistance = new Vector3(Random.Range(-30.0f, 110.0f), Random.Range(-100,100f), Random.Range(-100f, 100f));
		endPos = transform.position + (moveDistance);

		speed = 1f;



	}
	// Update is called once per frame

	private void OnMouseDown()
	{
		////Setting the default di

		Debug.Log("buttonDown");

		distanceCovered = 2f;
	}

	void Update () {
		
		//Incrementing the speed by time (Speed = Speed + deltaTime)
		speed += Time.deltaTime;

		//making the speed and distance the same float.
		if (speed > this.distanceCovered)
			{
				speed = this.distanceCovered;
			}

		


		float distanceCovered = (Time.time - startTime) * speed;




		percentage = speed / this.distanceCovered;


		transform.position = Vector3.Lerp(startPos, endPos, percentage);


	}
}
